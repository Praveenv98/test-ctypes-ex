#include <stddef.h>
#include <stdint.h>
#include <unistd.h>

#define _GNU_SOURCE
#include <stdio.h>
#include <caml/fail.h>
#include <caml/memory.h>
#include <caml/callback.h>
#include <caml/alloc.h>

#ifdef __linux__
# define HAVE_DLFCN_H 1
#endif
