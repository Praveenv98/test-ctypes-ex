let with_out_fmt filename f =
  let oc = open_out filename in
  let fmt = Format.formatter_of_out_channel oc in
  let finally () = close_out oc in
  let result =
    try f fmt with
      | Sys.Break as exn
      | exn -> begin
          close_out_noerr oc;
          raise exn
        end
  in
  finally ();
  result

let with_open_in filename f =
  let channel = open_in filename in
  f channel;
  close_in channel

let input fmt path =
  with_open_in path @@ fun fd ->
  try
    while true do
      Format.fprintf fmt "%s@." (input_line fd)
    done
  with End_of_file -> ()

(*refer to the binding module defined in the hello_rev_decl.ml*)

let () =
  let module Bindings = Hello_rev_decl.Rev_bindings in
  let filename_prefix = Sys.argv.(1) in
  let file ext = filename_prefix ^ ext in
  let prefix = "hello_rev" in

  let stubs_c = file "_stubs.c" in
  let stubs_h = file "_stubs.h" in
  let stubs_ml = file "_generated.ml" in

  begin
    with_out_fmt stubs_c (fun fmt ->
      input fmt "prelude.h";
        Cstubs_inverted.write_c fmt ~prefix (module Bindings);
      );

    with_out_fmt stubs_h (fun fmt ->
        input fmt "prelude.h";
        Cstubs_inverted.write_c_header fmt ~prefix (module Bindings);
      );

    with_out_fmt stubs_ml
      (fun fmt ->
         Cstubs_inverted.write_ml fmt ~prefix (module Bindings)
      );
end
