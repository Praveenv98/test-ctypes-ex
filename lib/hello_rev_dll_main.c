#include "prelude.h"

// explain why we need this file and it's function
//Needed to initialize the ocaml runtime when loading the shared object

__attribute__ ((__constructor__))
void
init(void)
{
  char *caml_argv[1] = { NULL };
  caml_startup(caml_argv);
}
