open Ctypes
(*open Foreign*)

module Hello = struct
  let hello s = Printf.sprintf "Hello %s" s

  type t = { msg : string }

  let init msg = { msg }
  let hello2 { msg } = hello msg

end

(* Fmt is used to return the formatted string to c code where they can be
 printed *)
let helloFmt (str:string) = Hello.hello str 

let helloFmt2 (str : string) =
	let tobject = Hello.init str 
		in Hello.hello2 tobject

module Rev_bindings (I : Cstubs_inverted.INTERNAL) = struct

  (* Add the binding here for the function Hello.hello and the function hello2 
   * defined above *)
  let () = I.internal "hello"    (string @-> returning string) helloFmt
  
  let () = I.internal "hello2"   (string @-> returning string) helloFmt2

end
