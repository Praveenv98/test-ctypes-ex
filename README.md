
## Exercise

Fill the following files following the instructions

- lib/decl/hello_rev_decl.ml
- tests/test.c

There are two small mistakes in the dune file and in the tests Makefile.
you will need to fix those to complete the exercise.

Bonus : explain step by step what is happening in the dune file
and the compilation workflow

### opam dependencies
    opam install ctypes ctypes-foreign


### System dependencies

    sudo apt install libffi-dev

## Compilation

    make
    make test
