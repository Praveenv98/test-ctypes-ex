The compilation flow:

make:

This project tries to build the final executable binary in stages.

At first, dune file in the directory lib/decl is used to create the 
library "hello_rev_decl" static library with dependencies from libraries -ctypes and ctypes.foreign
this library provides the ctypes bindings for the ocaml functions. This helps us to define c compatible 
functions to use our ocaml libraries.

Next, gen.ml module to compile to binary which is then used to generated the c source and header files 
with the help of bindings declared in decl/hello_rev_decl.ml and a hello_rev_generated.ml which used to
link the generated c code with the bindings declared.
The source file contains the implementation (wrapper around language boundaries) and header file contains the 
necessary interface to include the test.c without causing compilation failures.

Then, the library named "hello_rev" is created from the generated hello_rev_generated.ml ocaml module 
and mentions the c stub code created in the hello_rev_stubs.c.

And the hello_rev_dll_main.c is compiled to hello_rev_dll_main.o. This is needed because the init function 
in this file calls the caml_startup which is internally used to the initialize the ocaml runtime and static data.
This initialize happens before the 'C code main function' when loading a shared object at program startup.

Finally, the final executable shared object is prepared with libhello_rev.ml, hello_rev and link dependencies of
hello_rev_dll_main.o

install command - copy the files to the appropiate location in the _build directory.

make test:

compiles the test.c and at the program startup calls init function to initialize the ocaml runtime and dispatch 
to the correct c stubs interface which in turn directed to the corresponding ocaml function implementations and 
unload the shared object and return/exit.