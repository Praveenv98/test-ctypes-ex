#include "hellolib.h"
#include <stdio.h>

int main(int argc, char **argv) {

  // Add a simple test here to check if the binding are actually working
  //
  // the test file will get a string as argument and call
  // (hello s) and t = (init s) ; hello2 t
  //
  // for example ./test World
  // the result will be
  // Hello World
  // Hello World
  printf("\n -------------");
  printf("\n %s",hello(argv[1]));
  printf("\n %s",hello2(argv[1]));
  printf("\n -------------\n");  
  return EXIT_SUCCESS;
}
